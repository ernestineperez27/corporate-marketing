**STEP ONE:** Title this request: Brand Review [Item]

## Overview
#### What Team was this made by
`Requestor Fill Out`

#### Was this done by an agency/vendor?
* [ ] No
* [ ] Yes : `Agency`

#### Did our design team consult on this prior to this review?
* [ ] No
* [ ] Yes - Link Relevant issue notes

#### Where will this be used?
* [ ] Internal Only (no one outside GitLab)
* [ ] External

#### Is there a due date and why?
* `MM-DD-YYYY`
* `WHY`

If **External** provide specific information (what channels etc.)
`Requestor Fill Out`

## Relevant Links/Documents
Please Link or upload item to be reviewed
* Item to be reviewed (LINK)

If you used a self-serve resource please link handbook item here:
* Self-Serve Link

____________
For design team only

## Review Status
To be filled out by Design Team Reviewer
* [ ] Reviewed without comment
* [ ] Reviewed with comment (comment in issue)
* [ ] Request this not move forward (comment in issue)

DESIGN TEAM Check one and re-assign to requestor






/label ~"design" ~"mktg-status::triage" ~"Corporate Marketing"

/assign @Emily
