<!-- This Issue Template is for requesting NEW design assets only. This is NOT for campaigns which require complex deliverables and concepting.
Please see handbook page: LINK (coming soon) for more details, instructions and guidance.

STEP ONE: Name this issue: DESIGN REQUEST: [Descriptive name]
-->
## Overview

#### Asset Overview
Please describe why you want this asset made

#### Overview of  Overarching Project
Please describe the overall project these assets will be used in

#### Objective of Overarching Project
What is your team trying to accomplish? What problems are you trying to solve for?

#### Summary of User Journey
* Where did they come from that they will see this asset?
  - `Information`
* Where will they be going next? What do we want the action to be from viewing this asset?
  - `Information`
#### DRIs and Review
Who is the DRI for this project on your side and who needs to review?

|Name|Role|
|------|------|
| `@yourhandle` | Requestor |
| `@user2` | `ROLE` |
| `@user3` | `ROLE` |
________________
## Audience

Who is primarily looking at these designs AND what will they they be doing with them/wanting to learn from them.Write specifics AND check off below anything relevant

**Specific Details**
<!-- Write in specifics. Example: People trying to understand all the features of GitLab",  "event attendees looking to sign up for a demo", "international - first time running ads in that country"
-->
`SPECIFICS`

**Person**
* [ ] Individual (Developer, Software Engineer enjoying GItLab at event or seeking more information on features)
* [ ] Person representing company (looking for a solution for DevOps, getting a trial, etc)
* [ ] PR/Media
* [ ] Investors/Board members

**How much do they know about GitLab?**
This will help determine the prevalence of logo usage tone
* [ ] Very new or first time visiting (most likely came from ads or search)
* [ ] Has been here a few times
* [ ] Very familiar (likely looking for documentation, resources, new features, community projects, etc. )

**Special Considerations (check if applicable):**

* [ ] Government
* [ ] Outside U.S. (primary)
* [ ] Direct Competitive (ex: OctoCat Campaign}
* [ ] Partner

______________
## Deliverable Information

#### Design Request Category
* [ ] Individual Asset/Icon
* [ ] Inforgraphic
* [ ] Email Graphics (ex: Banner/Header)
* [ ] Co-branded Logo
* [ ] Logo (note unless there is a true business valueto IACV these may be backlogged)
* [ ] Overall Brand Look and Feel (what colors we should use, the style, etc.)
* [ ] Event Booth
* [ ] Swag design
* [ ] Other - Please specify

#### Design Applications
Intended Use to help us understand Specs
* [ ] Paid Display ads
* [ ] Paid Social media
* [ ] Organic Social Media
* [ ] Email
* [ ] Web banner
* [ ] Other Please list

#### Due Date
Please put the due date  for final delivery of assets and why.

#### Deliverables
* [ ] Deliverable 1
* [ ] Deliverable 2

<!--
Please note: Most regular design requests take 2 weeks; without sufficient lead time we will likely be unable to meet your deadline. Note, there is a production work-back schedule that will need to be followed.

Example: Our due date is XXXX-XX-XX. We are launching a campaign on MM.DD.YYYY and web development requires final assets by MM.DD.YYYY
-->
____________________
THIS SECTION IS FOR DESIGN TEAM ONLY

## Production Schedule
- Brand direction - Luke MM.DD.YYYY
- Concepts: MM.DD.YYY
- Round 1: MM.DD.YYY
- Feedback from requestor: MM.DD.YYY
- Round 2: MM.DD.YYY
- Final feedback by requestor: MM.DD.YYY
- Final Assets delivered (Due date) MM.DD.YYY

This ask does not require a child epic. Please follow practices for production rounds in the Handbook LINK (coming soon).

/label ~"design" ~"mktg-status::triage" ~"Corporate Marketing"

/assign @Emily
